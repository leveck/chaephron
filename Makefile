#
# Makefile
# leveck, 2020-01-15 21:47
#

clean:
	rm chaephron

install:
	fbc chaephron.bas
	cp chaephron /usr/local/bin
	cp chaephron.1 /usr/local/share/man/man1/

uninstall:
	rm /usr/local/bin/chaephron
	rm /usr/local/share/man/man1/chaephron.1

# vim:ft=make
#
