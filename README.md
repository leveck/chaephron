# Chæphron

*Chæphron* is a WIP gopher client written in FreeBasic. At the moment it depends on netcat, sed, nl, and less. *Chæphron* is no longer hardcoded to port 70 and will follow links on other ports.

The information in this README is also in the included manpage.

## The Name

Chæphron is named after an album I released in 2015 for my project Natanas.

## Installation

To install:

Ensure you have the FreeBasic compiler (fbc), netcat, sed, nl, and less installed. It *should* compile on Unixes. Even if it did compile on Windows, it likely would not work correctly.

```
sudo make install
```

## Uninstallation

To uninstall:

```
sudo make uninstall
```

## Usage

```
$ chaephron domain.tld/1/path
```

Note: do not include to URL scheme (gopher://). Chæphron ignores item type. Gopher selectors are stripped from the display are treated the same. You should likely avoid visiting any URLs besides type 1 and 0, as the raw file contents will display on the screen.

Inside Chæphron, less is invoked as a pager. All lines are numbered with nl. Hit ```q``` to exit less, then type the number of the line with a link to the next destination and hit return to visit that link. Enter ```?``` to view help. Enter ```l``` to view the raw gopher response. Enter ```b``` to go back (255 levels of history). Enter ```g``` to be prompted for a new URL. Enter ```q``` instead at the $ prompt to quit Chæphron.

## Screenshot

![](screenshot.png)
