'Chæphron - a gopher client
'version 0.8
'(c) 2020 Nathaniel Leveck

dim shared as string a, gurl, port, pat, cmdstr, past(255), pg
dim shared as boolean glow
gurl = Command(1)
port = Command(2)
'the pager to use
pg = "less -Xe"
'set to true to use glow as markdown pager, false for pg above
glow = TRUE

if port = "" then
  port = "70"
end if

sub hist(dire as string)
  dim as integer x
  if dire = "f" then
    for x = 254 to 1 step -1
      past(x) = past(x-1)
    next
    past(0) = gurl & "/%" & pat
  else
    for x = 0 to 254
      past(x) = past(x+1)
    next
  end if
end sub

sub go()
  gurl = ""
  print "Enter a gopher URL:"
  input "gopher://",gurl
  hist("f")
end sub

sub help()
  dim as string tmp
  print "Chæphron a gopher protocol client"
  print "by Nathaniel Leveck 2020, 2022"
  print
  print "Launching Chæphron:"
  print "$ chaephron URL-with-no-scheme port "
  print
  print "Inside Chæphron, less is invoked as a pager. "
  print "All lines are numbered with nl. "
  print "Hit q to exit less, then type the number of the "
  print "  line with a link to the next destination and "
  print "  hit return to visit that link. "
  print "Enter ? to see this help. "
  print "Enter d to save the current item to a file. "
  print "Enter l to view the raw gopher response. "
  print "Enter b to go back (255 levels of history). "
  print "Enter g to be prompted for a new URL."
  print "Enter q instead at the $ prompt to quit Chæphron. "
  if gurl <> "-h" and gurl <> "--help" then
    input "Press Enter to continue.", tmp
  end if
  print
end sub

sub comm()
  dim as string comnd
  input "$ ",comnd
  select case comnd
    case "g"
      go
    case "?"
      help
    case "b"
      if past(0) <> "" then
        hist("b")
        gurl = past(0)
      else
        print "No history."
      end if
    case "d"
      shell "cp /tmp/t ~/csave$(date '+%Y%m%d%H%M%S').txt"
    case "l"
      shell pg & " /tmp/t"
    case "q"
      gurl = comnd
    case else
      if val(comnd) >= 1 then
        hist("f")
        dim as integer i = 1
	dim as string a, p, x, y ,z
        open "/tmp/t" for input as #1
          do
	    line input #1, a
            if i = val(comnd) then
              x = mid(a, instr(instr(a, chr(9)) + 2,a, chr(9)) + 1)
	      y = mid(x, 1, instr(x, chr(9)) -1)
	      z = mid(x, instr(x, y) + len(y))
	      z = mid(z, 2, instr(2,z, chr(9)) -2)
	      p = mid(a, instr(instr(a, z)+5, a, chr(9)), 5)
	      gurl = z & "/1" & y
	      'port = p
	    end if
	    i += 1
	  loop until eof(1)
        close #1
      end if
  end select
end sub

sub doit()
  if gurl <> "q" then
    if instr(gurl, "/") then
      pat = mid(gurl, instr(gurl, "/") + 2, len(gurl))
      gurl = mid(gurl, 1, instr(gurl, "/") -1)
    else
      pat = "/"
    end if

    if glow = TRUE then
      if right(pat,2) = "md" or right(pat,4) = "post" or right(pat,3) = "org" or right(pat,3) = "txt" then
        cmdstr = "printf " & chr(34) & pat & "\n" & chr(34) & " | nc " & gurl & " " & port & " > /tmp/t && sed 's/\([0-9]*\t.*\)\t.*\t.*\t.*/\1/g;s/\ti/\t/g' /tmp/t | glow -p -w 70 -"
      else
        cmdstr = "printf " & chr(34) & pat & "\n" & chr(34) & " | nc " & gurl & " " & port & " | nl > /tmp/t && sed 's/\([0-9]*\t.*\)\t.*\t.*\t.*/\1/g;s/\ti/\t/g' /tmp/t | " & pg
      end if
    else
      cmdstr = "printf " & chr(34) & pat & "\n" & chr(34) & " | nc " & gurl & " " & port & " | nl > /tmp/t && sed 's/\([0-9]*\t.*\)\t.*\t.*\t.*/\1/g;s/\ti/\t/g' /tmp/t | " & pg
    end if
    shell cmdstr
    comm
  end if
end sub

if gurl = "-h" or gurl = "--help" then
  help
  end
end if

do
  doit
loop until gurl = "q"

kill "/tmp/t"
